module.exports = {
    meta: {
        dokümanlar: {
            description: 'html sınıf isimleri `` u-`veya` kiwi-' 'ile başlamalıdır,
            kategori: 'taban',
            url: boş,
        },
        düzeltilebilir: boş,
        şema: [],
    },
    create: function (bağlam) {
        return context.parserServices.defineTemplateBodyVisitor ({
            "VAttribute [key.name = 'class']" (düğüm) {
                let sınıflar = node.value.value.split ('');
                classes.forEach ((c) => {
                    // Boş ve fontawesome sınıflarını yoksay
                    eğer (! c || c === 'fa' || c.startsWith ('fa-')) {
                        dönüş;
                    }
                    eğer (! c.startsWith ('kivi-') &&
                        ! c.startsWith ('u-') &&
                        // google recaptcha için özel istisna - hoş geldiniz ekranı.
                        ! c.startsWith ('g-') &&
                        ! c.startsWith ('irc-fg-') &&
                        ! c.startsWith ('irc-bg-')
                    ) {
                        context.report ({
                            düğüm: düğüm,
                            message: 'Expected class name to start with `kiwi-` or `u-` ({{ class }})',
                            data: {
                                class: c,
                            },
                        });
                    }
                });
            },
        });
    },
};
